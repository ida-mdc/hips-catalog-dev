# hips-catalog development repository

This repository hosts the source code of all HIP solutions (HIPS) developed at the Image Data Analysis group at MDC Berlin.
Release versions will be deployed to https://gitlab.com/ida-mdc/hips-catalog. 